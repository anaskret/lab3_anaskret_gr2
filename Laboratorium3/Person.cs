﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorium3
{
    public class Person
    {
        private long id;
        private string firstName;
        private string lastName;

        public string FirstName
        {
            get
            {
                return FirstName;
            }
            set
            {
                FirstName = firstName;
            }
        }
        public string LastName
        {
            get
            {
                return LastName;
            }
            set
            {
                LastName = lastName;
            }
        }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }

        public Person(long id)
        {
            this.id = id;
        }
        
        public Person(long id, string firstName, string lastName) : this(id)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Person(long id, string firstName, string lastName, string gender) : this(id, firstName,lastName)
        {
            this.Gender = gender;
        }

        public void GetFullName()
        {
            Console.WriteLine($"Imie: {firstName}, Nazwisko: {lastName}");
        }
        
        public void GetAge()
        {
            var today = DateTime.Today;
            var age = today.Year - DateOfBirth.Year;
            Console.WriteLine($"Wiek: {age} lat");
        } 

        public bool CheckId(long pesel)
        {
            return this.id == pesel ? true : false;
        }
    }
}
