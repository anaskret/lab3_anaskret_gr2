﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorium3
{
    class Program
    {
        static bool CheckString(string check)
        {
            if(check.All(char.IsLetter)) return true;
            else 
            {
                Console.WriteLine("Wpisz jeden wyraz i same litery");
                return false;
            }
        }

        static void Main(string[] args)
        {
            long id;
            while(true)
            {
                try
                {
                    Console.WriteLine("Podaj pesel: ");
                    id = Int64.Parse(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("Pesel musi zawierać 11 cyfr");
                    continue;
                }
                if(id>10000000000 && id <= 99999999999) break;
                else Console.WriteLine("Pesel musi zawierac 11 cyfr");
            }

            string firstName;
            do
            {
                Console.WriteLine("Podaj imie: ");
                firstName = Console.ReadLine();
            }while(!CheckString(firstName));

            string lastName;
            do
            {
            Console.WriteLine("Podaj nazwisko: ");
            lastName = Console.ReadLine();
            }while(!CheckString(lastName));
            string gender;
            do
            {
                Console.WriteLine("Podaj płec (M/F): ");
                gender = Console.ReadLine();
                gender = gender.ToUpper();
            }while(gender != "M" && gender != "F");
            
            int day, month, year;
            while(true)
            {
                try
                {
                    Console.WriteLine("Podaj rok urodzin: ");
                    year = Int32.Parse(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("Wpisz tylko cyfry");
                    continue;
                }
                if(year>=1903 && year <= 2019) break;
                else Console.WriteLine("Rok musi byc pomiedzy 1903 a 2019");
            }

            while(true)
            {
                try
                {
                    Console.WriteLine("Podaj miesiac urodzin: ");
                    month = Int32.Parse(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("Wpisz tylko cyfry");
                    continue;
                }
                if(month>=1 && month <= 12) break;
                else Console.WriteLine("Miesiąc musi byc pomiedzy 1 a 12");
            }
            
            int daysInMonth = System.DateTime.DaysInMonth(year,month);

            while(true)
            {
                try
                {
                    Console.WriteLine("Podaj dzien urodzin: ");
                    day = Int32.Parse(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("Wpisz tylko cyfry");
                    continue;
                }
                if(day>=1 && day<=daysInMonth) break;
                else Console.WriteLine($"Dzien musi byc pomiedzy 1 a {daysInMonth}");
            }

            var osoba = new Person(id, firstName, lastName, gender);

            osoba.DateOfBirth = new DateTime(year, month,day);
            
            int petla = 0;
            
            while(petla!=1)
            {
                Console.WriteLine("");
                Console.WriteLine("***Menu***");
                Console.WriteLine("1. Pokaz Imie i Nazwisko");            
                Console.WriteLine("2. Pokaz wiek");            
                Console.WriteLine("3. Sprawdz pesel");
                Console.WriteLine("4. Koniec");            
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                switch(x)
                {
                    case 1:
                        osoba.GetFullName();
                        break;
                    case 2:
                        osoba.GetAge();
                        break;
                    case 3:
                        long pesel;
                        while(true)
                        {
                            try
                            {
                                Console.WriteLine("Podaj pesel do porównania: ");
                                pesel = Int64.Parse(Console.ReadLine());
                            }
                            catch(FormatException)
                            {
                                Console.WriteLine("Pesel musi zawierać 11 cyfr");
                                continue;
                            }
                            if(pesel>10000000000 && pesel <= 99999999999) break;
                            else Console.WriteLine("Pesel musi zawierac 11 cyfr");
                        }

                        if(osoba.CheckId(pesel)==true)
                        {
                            Console.WriteLine("Pesel sie zgadza");
                        }
                        else Console.WriteLine("Pesel sie nie zgadza");
                        break;
                    case 4:
                        petla=1;
                        break;
                    default:
                        Console.WriteLine("Wybrales zla opcje!");
                        break;
                }
                
            }
        }
    }
}
